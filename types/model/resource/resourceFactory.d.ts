import Resource from "./Resource";
export default function resourceFactory(resourcePath: string): Resource | undefined;
