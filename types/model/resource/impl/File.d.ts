import Resource from "../Resource";
export default class File extends Resource {
    private static readonly FILE_CHAR;
    private static readonly FILE_PAD_CHAR;
    constructor(path: string, name: string, size: number, modificationTime: Date, creationTime: Date);
    treeString(_isLast: boolean): string;
}
