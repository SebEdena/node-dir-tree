import Resource from "../Resource";
export default class Directory extends Resource {
    private static readonly DIR_CHAR;
    private static readonly LAST_DIR_CHAR;
    private static readonly DIR_PAD_CHAR;
    constructor(path: string, name: string, modificationTime: Date, creationTime: Date);
    treeString(isLast: boolean): string;
    get size(): number;
}
