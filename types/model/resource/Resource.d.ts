import { ResourceType } from "./ResourceType";
export default abstract class Resource {
    protected static readonly PAD_LENGTH = 4;
    protected _parentDir: string;
    protected _name: string;
    protected _size: number;
    protected _modificationTime: Date;
    protected _creationTime: Date;
    protected _type: ResourceType;
    protected _children: Array<Resource>;
    constructor(parentDir: string, name: string, size: number, modificationTime: Date, creationTime: Date, type: ResourceType);
    get parentDir(): string;
    get name(): string;
    get size(): number;
    get modificationTime(): Date;
    get creationTime(): Date;
    get type(): ResourceType;
    get children(): Array<Resource>;
    set children(children: Array<Resource>);
    abstract treeString(isLast: boolean): string;
    getPath(): string;
}
