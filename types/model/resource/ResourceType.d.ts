/// <reference types="node" />
import { Stats } from "fs";
export declare enum ResourceType {
    DIRECTORY = 0,
    FILE = 1
}
export declare function getResourceType(stat: Stats): ResourceType | undefined;
