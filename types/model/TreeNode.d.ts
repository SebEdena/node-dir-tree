import Resource from "./resource/Resource";
export default class TreeNode {
    path: string;
    resource: Resource;
    children: Array<TreeNode>;
    constructor(path: string, resource: Resource);
}
