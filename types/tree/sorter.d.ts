import Resource from "../model/resource/Resource";
import { SortType } from "../sort/SortType";
declare type SortOptions = {
    sort: SortType;
    reverse: boolean;
};
export declare function sortTree(resource: Resource, options: Partial<SortOptions>): void;
export {};
