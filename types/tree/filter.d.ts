import Resource from "../model/resource/Resource";
declare type FilterOptions = {
    files: boolean;
    depth: number;
    ignore: Array<string>;
};
export default function filterTree(resource: Resource, options: Partial<FilterOptions>): void;
export {};
