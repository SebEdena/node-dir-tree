export declare const sortValues: readonly ["name", "size", "creation_date", "modification_date"];
declare type SortTypeArray = typeof sortValues;
export declare type SortType = SortTypeArray[number];
export {};
