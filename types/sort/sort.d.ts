import Resource from "../model/resource/Resource";
import { SortType } from "./SortType";
export declare type SortingMethod = (a: Resource, b: Resource) => number;
export default function getSort(sort: SortType, reverse: boolean): SortingMethod;
