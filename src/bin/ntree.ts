#!/usr/bin/env node

import * as yargs from "yargs";
import buildTree from "../tree/builder";
import printTree from "../tree/printer";
import { SortType, sortValues } from "../sort/SortType";
import filterTree from "../tree/filter";
import { sortTree } from "../tree/sorter";
const version : string = require("../../package.json").version;

yargs
    .scriptName("ntree")
    .version(version)
    .alias("v", "version")
    .help()
    .alias("h", "help")
    .command(
        "* <path>",
        "Print a directory structure",
        (args) =>
            args
                .positional(
                    "path",
                    {
                        description: "The path to print",
                        type: "string",
                        default: ".",
                        normalize: true
                    }
                )
                .options({
                    "files": {
                        alias: "f",
                        description: "Display files",
                        type: "boolean",
                        default: false,
                    },
                    "depth": {
                        alias: "d",
                        description: "Set the maximum depth of the structure",
                        type: "number",
                        default: Number.MAX_SAFE_INTEGER
                    },
                    "sort": {
                        alias: "s",
                        description: "Define the sorting method",
                        type: "string",
                        default: "name",
                        choices: sortValues
                    },
                    "ignore": {
                        alias: "i",
                        description: "Ignore files or folders according to globs",
                        type: "array",
                        default: []
                    },
                    "reverse": {
                        alias: "r",
                        description: "Reverse the order of sort",
                        type: "boolean",
                        default: false
                    }
                }),
        ({ path, files, depth, sort, ignore, reverse }) => {
            try {
                const resource = buildTree(path);
                filterTree(resource, { files, depth, ignore });
                sortTree(resource, { sort: sort as SortType, reverse });
                console.log(printTree(resource));
            } catch (error) {
                console.error(error.message);
            }
        }
    )
    .wrap(yargs.terminalWidth())
    .parse();
