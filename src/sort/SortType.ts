export const sortValues = ["name", "size", "creation_date", "modification_date"] as const;
type SortTypeArray = typeof sortValues;
export type SortType = SortTypeArray[number];
