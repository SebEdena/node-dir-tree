import Resource from "../model/resource/Resource";
import { SortType } from "./SortType";

export type SortingMethod = (a: Resource, b: Resource) => number;

const typeSorter : SortingMethod = (a: Resource, b: Resource) =>
    a.type - b.type;

const nameSorter : SortingMethod = (a: Resource, b: Resource) =>
    a.name.localeCompare(
        b.name,
        undefined,
        {
            sensitivity: "base",
            usage: "sort"
        }
    );

const sizeSorter : SortingMethod = (a: Resource, b: Resource) =>
    a.size - b.size;

const cdateSorter : SortingMethod = (a: Resource, b: Resource) =>
    a.creationTime.getTime() - b.creationTime.getTime();

const mdateSorter : SortingMethod = (a: Resource, b: Resource) =>
    a.modificationTime.getTime() - b.modificationTime.getTime();

function createSorter(sorters : SortingMethod[], reverse : boolean) : SortingMethod {
    return (a: Resource, b: Resource) => {
        for (const sorter of sorters) {
            const sortResult = sorter(a, b);
            if (sortResult !== 0) return sortResult * (reverse ? -1 : 1);
        }
        return 0;
    };
}

export default function getSort(sort: SortType, reverse: boolean) : SortingMethod {

    switch (sort) {
        case "name": return createSorter([typeSorter, nameSorter], reverse);
        case "size": return createSorter([typeSorter, sizeSorter, nameSorter], reverse);
        case "creation_date": return createSorter([typeSorter, cdateSorter, nameSorter], reverse);
        case "modification_date": return createSorter([typeSorter, mdateSorter, nameSorter], reverse);
        default: return createSorter([typeSorter, nameSorter], reverse);
    }
}
