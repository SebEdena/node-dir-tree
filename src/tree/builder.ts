import fs from "fs";
import path from "path";
import resourceFactory from "../model/resource/resourceFactory";
import { ResourceType } from "../model/resource/ResourceType";
import Resource from "../model/resource/Resource";

export default function buildTree(rootPath: string): Resource {

    const absoluteRootPath: string = path.resolve(rootPath);
    const root = resourceFactory(absoluteRootPath);

    if (!root) throw new Error("Root path does not exist!");
    if (root.type !== ResourceType.DIRECTORY) throw new Error("Root path must be a directory!");

    const stack = [root];

    while (stack.length) {
        const currentNode = stack.pop();

        if (currentNode) {
            const currentNodePath = currentNode.getPath();
            const children = fs.readdirSync(currentNodePath);

            for (const child of children) {
                const childPath = path.join(currentNodePath, child);
                const childNode = resourceFactory(childPath);

                if (childNode) {
                    if (childNode.type === ResourceType.DIRECTORY) {
                        currentNode.children.push(childNode);
                        stack.push(childNode);
                    } else if (childNode.type === ResourceType.FILE) {
                        currentNode.children.push(childNode);
                    }
                }
            }
        }

    }

    return root;
}
