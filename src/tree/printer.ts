import { EOL } from "os";

import Resource from "../model/resource/Resource";
import { ResourceType } from "../model/resource/ResourceType";

const DEPTH_PAD = "|";
const PAD_SIZE = 4;
const PAD_CHAR = " ";

export default function print(tree: Resource) : string {
    return printNode(tree, "", 0, 0, []);
}

function printNode(
    resource: Resource,
    accumulator: string,
    parentDepth: number,
    depth: number,
    lasts: boolean[]
): string {
    let stringTree = accumulator;
    const isLast : boolean = lasts[lasts.length - 1];
    if (depth === 0) {
        stringTree += resource.getPath() + EOL;
    } else {
        for (let i = 0; i < Math.max(lasts.length - 1, 0); i++) {
            stringTree += (lasts[i] ? PAD_CHAR : DEPTH_PAD).padEnd(PAD_SIZE, PAD_CHAR);
        }
        stringTree += resource.treeString(isLast) + EOL;
    }
    for (let i = 0; i < resource.children.length; i++) {
        stringTree +=
            printNode(
                resource.children[i],
                accumulator,
                isLast ? parentDepth : depth,
                depth + 1,
                [...lasts, isLastDir(resource.children, i)]
            );
    }
    return stringTree;
}

function isLastDir(children: Resource[], index: number) : boolean {
    if (children[index].type === ResourceType.DIRECTORY) {
        for (let i = index + 1; i < children.length; i++) {
            if (children[i].type === ResourceType.DIRECTORY) {
                return false;
            }
        }
        return true;
    }
    return false;
}
