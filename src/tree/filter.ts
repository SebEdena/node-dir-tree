import M, { Minimatch } from "minimatch";
import Resource from "../model/resource/Resource";
import { ResourceType } from "../model/resource/ResourceType";


type FilterOptions = {
    files: boolean;
    depth: number;
    ignore: Array<string>;
};

const defaultOptions: FilterOptions = {
    files: false,
    depth: Number.MAX_SAFE_INTEGER,
    ignore: []
};

export default function filterTree(resource: Resource, options: Partial<FilterOptions>): void {

    const { files, depth, ignore } = { ...defaultOptions, ...options };

    const resourceTypes = [ResourceType.DIRECTORY];
    if (files) resourceTypes.push(ResourceType.FILE);

    const ignoreMatchers: M.IMinimatch[] = ignore.map((ignoreGlob) => new Minimatch(ignoreGlob));

    filter(resource, resourceTypes, 0, depth, ignoreMatchers);
}

function filter(
    resource: Resource,
    types: Array<ResourceType>,
    depth: number,
    maxDepth: number,
    ignoreMatchers: Array<M.IMinimatch>
): void {

    resource.children = resource.children.filter((child) => {
        let keepResource = child.type in types && depth <= maxDepth;
        if (keepResource) {
            for (const ignoreMatcher of ignoreMatchers) {
                keepResource = keepResource && !ignoreMatcher.match(child.getPath());
                if (!keepResource) break;
            }
        }
        return keepResource;
    });

    resource.children.forEach((child) => filter(child, types, depth + 1, maxDepth, ignoreMatchers));
}
