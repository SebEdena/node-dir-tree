import Resource from "../model/resource/Resource";
import { SortType } from "../sort/SortType";
import sorter, { SortingMethod } from "../sort/sort";

type SortOptions = {
    sort: SortType;
    reverse: boolean;
};

const defaultOptions : SortOptions = {
    sort: "name",
    reverse: false
};

export function sortTree(resource: Resource, options: Partial<SortOptions>): void {

    const { sort, reverse } = { ...defaultOptions, ...options };
    const sortingMethod = sorter(sort, reverse);

    sortResource(resource, sortingMethod);

}

function sortResource(resource: Resource, sortingMethod: SortingMethod): void {
    resource.children.sort(sortingMethod);
    resource.children.forEach((child) => sortResource(child, sortingMethod));
}
