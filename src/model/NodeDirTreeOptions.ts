import { SortType } from "../sort/SortType";

export default interface NodeDirTreeOptions {
    files: boolean;
    sort: SortType;
    depth: number;
}
