import { ResourceType } from "./ResourceType";
import { join } from "path";

export default abstract class Resource {

    protected static readonly PAD_LENGTH = 4;

    protected _parentDir: string;

    protected _name: string;

    protected _size: number;

    protected _modificationTime: Date;

    protected _creationTime: Date;

    protected _type: ResourceType;

    protected _children: Array<Resource>;

    public constructor(
        parentDir: string,
        name: string,
        size: number,
        modificationTime: Date,
        creationTime: Date,
        type: ResourceType
    ) {
        this._parentDir = parentDir;
        this._name = name;
        this._size = size;
        this._modificationTime = modificationTime;
        this._creationTime = creationTime;
        this._type = type;
        this._children = [];
    }

    get parentDir(): string {
        return this._parentDir;
    }

    get name(): string {
        return this._name;
    }

    get size(): number {
        return this._size;
    }

    get modificationTime(): Date {
        return this._modificationTime;
    }

    get creationTime(): Date {
        return this._creationTime;
    }

    get type(): ResourceType {
        return this._type;
    }

    get children(): Array<Resource> {
        return this._children;
    }

    set children(children: Array<Resource>) {
        this._children = children;
    }

    public abstract treeString(isLast: boolean): string;

    public getPath(): string {
        return join(this._parentDir, this._name);
    }

}
