import { Stats } from "fs";

// eslint-disable-next-line no-shadow
export enum ResourceType {
    DIRECTORY,
    FILE
}

export function getResourceType(stat: Stats) : ResourceType | undefined {
    if (stat.isDirectory()) return ResourceType.DIRECTORY;
    if (stat.isFile()) return ResourceType.FILE;
    return undefined;
}
