import path from "path";
import fs from "fs";
import { ResourceType, getResourceType } from "./ResourceType";
import Directory from "./impl/Directory";
import File from "./impl/File";
import Resource from "./Resource";

export default function resourceFactory(resourcePath: string) : Resource | undefined {
    try {
        const info = path.parse(resourcePath);
        const metadata = fs.statSync(resourcePath);
        const resourceType = getResourceType(metadata);

        switch (resourceType) {
            case ResourceType.DIRECTORY: {
                return new Directory(
                    info.dir,
                    info.base,
                    metadata.mtime,
                    metadata.ctime
                );
            }
            case ResourceType.FILE: {
                return new File(
                    info.dir,
                    info.base,
                    metadata.size,
                    metadata.mtime,
                    metadata.ctime
                );
            }
            default: return undefined;
        }
    } catch (error) {
        return undefined;
    }
}
