import Resource from "../Resource";
import { ResourceType } from "../ResourceType";

export default class Directory extends Resource {

    private static readonly DIR_CHAR = "+";

    private static readonly LAST_DIR_CHAR = "\\";

    private static readonly DIR_PAD_CHAR = "-";

    public constructor(
        path: string,
        name: string,
        modificationTime: Date,
        creationTime: Date
    ) {
        super(
            path,
            name,
            -1,
            modificationTime,
            creationTime,
            ResourceType.DIRECTORY
        );
    }

    public treeString(isLast: boolean): string {
        const firstChar = (isLast ? Directory.LAST_DIR_CHAR : Directory.DIR_CHAR);
        const start = firstChar.padEnd(Resource.PAD_LENGTH, Directory.DIR_PAD_CHAR);
        return start + this._name;
    }

    get size(): number {

        if (this._size < 0) {
            let size = 0;
            for (const child of this.children) {
                size += child.size;
            }
            this._size = size;
        }
        return this._size;
    }

}
