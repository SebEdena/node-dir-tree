import Resource from "../Resource";
import { ResourceType } from "../ResourceType";

export default class File extends Resource {

    private static readonly FILE_CHAR = " ";

    private static readonly FILE_PAD_CHAR = " ";

    public constructor(
        path: string,
        name: string,
        size: number,
        modificationTime: Date,
        creationTime: Date
    ) {
        super(
            path,
            name,
            size,
            modificationTime,
            creationTime,
            ResourceType.FILE
        );
    }

    public treeString(_isLast: boolean): string {
        const start = (File.FILE_CHAR).padEnd(Resource.PAD_LENGTH, File.FILE_PAD_CHAR);
        return start + this._name;
    }

}
