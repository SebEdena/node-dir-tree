import Resource from "./resource/Resource";

export default class TreeNode {

    public path: string;

    public resource: Resource;

    public children: Array<TreeNode>;

    constructor(path: string, resource: Resource) {
        this.path = path;
        this.resource = resource;
        this.children = [];
    }
}
